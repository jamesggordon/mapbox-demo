![Archistar](https://archistar.ai/wp-content/uploads/2019/08/archi.svg)

# Archistar Front-end Coding Challenge Solution (about 5% thereof :-)

Details of this challenge can be found [here](https://bitbucket.org/idda/coding-challenges/src/master/Sr-Software-Engineer-FE.md).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## What Works

Well, the map renders, the geoJSON data is retrieved using ``axios`` and markers are added to the map for each property in the file. The layout is roughly correct (sidebar on the left, ready to house the search controls, with the map on the right) and there are no horrible errors in the console. That's about it really! 😀

## What Doesn't Work

Just about everything else in the requirements! I hard-coded the co-ordinates to centre the map rather than calculating them on the fly, which is obviously not a good solution. I also hard-coded my Mapbox API key into the code, which is super-bad practice and, as such, I will be regenerating the token soon.

## What Should Be Done

I am not a Vue expert, but from what I understand the application should probably consist of:

1. A component to structure the overall app (could use ``App`` for that I guess).
2. Each filter control should be its own component.
3. There should be another component and template to house the filter components and look after their layout.
4. A component to handle the map.
5. Vuex to manage state and communications between all of the components.
6. Unit tests, at least for all of the mutations and other statement management code.
7. Externalising the API key!
